# About me

I'm originally from Italy, but currently based in the Netherlands for my engineering studies. I like to have control over the things I own and use, which is why:

- I use  Linux (currently [openSUSE Tumbleweed](https://get.opensuse.org/tumbleweed/));
- I self-host some services on a [Raspberry Pi](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/), using [Podman](https://podman.io/ "podman.io") and [Tailscale](https://tailscale.com/);
- I code some small projects, mainly in [Go](https://go.dev "go.dev");
- I have a personal website, at [bikobi.gitlab.io](https://bikobi.gitlab.io);
- I support [right-to-repair](https://en.wikipedia.org/wiki/Right_to_repair).

## Quick links

- My website: [bikobi.gitlab.io](https://bikobi.gitlab.io)
- My dotfiles: [gruvbox bspwm+polybar](https://gitlab.com/bikobi/dotfiles)
- My latest project: [repo](https://gitlab.com/bikobi/wordle) | [demo](https://swordle.fly.dev)

